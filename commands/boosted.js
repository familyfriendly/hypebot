
var moment = require('moment');

exports.run = function(client, message, args)  {
    if(!args[0]) return message.channel.send("please attatch a member");
    let user = message.mentions.members.first() || message.guild.members.get(args[0]);
    if(client.data.exists(user.id)) return message.channel.send(`user already exists in database...`);
    user.addRole("577232320277381121");
    let date = moment();
    date = date.add(1, "week");
    date = date.format('L');
    client.data.write(user.id,date);
    message.channel.send(`done! ${user} will have premium features until \`${date}\``);
 }

module.exports.config = {
    name: "boosted",
    aliases: ["boost"],
      usage: "b!boosted <user>",
    description: "add a user to the boosted role granting them premiums",
    acclvl: 2
    }