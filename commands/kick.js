exports.run = function(client, message, args)  { 
    let member = message.mentions.members.first() || message.guild.members.get(args[0])
    if(!member) return;
    if(!member.bannable) 
      return message.kickable("member unkickable");

    let reason = args.slice(1).join(' ');
     member.kick(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't kick because of : ${error}`));


      client.action(member.user,"kick",message,reason)

}

module.exports.config = {
    name: "kick",
    aliases: [],
      usage: "!kick <user> ?reason",
    description: "yeet them out of the server",
    acclvl: 2
    }