exports.run = function(client, message, args)  { 
    let member = message.mentions.members.first() || message.guild.members.get(args[0])
    if(!member) return;
    if(!member.bannable) 
      return message.reply("member unbannable");

    let reason = args.slice(1).join(' ');
     member.ban(reason)
      .catch(error => message.reply(`Sorry ${message.author} I couldn't ban because of : ${error}`));


      client.action(member.user,"ban",message,reason)

}

module.exports.config = {
    name: "ban",
    aliases: [],
      usage: "!ban <user> ?reason",
    description: "yeet them out of the server",
    acclvl: 2
    }