
exports.run = function(client, message, args)  { 

    let sql = `SELECT * FROM \`userdata\` WHERE id = ${message.author.id}`
    client.db.query(sql, (err, res) => {
        if (err) return message.channel.send(`we got an error: ${err}`);
        if((res+"") === "") return;
        message.channel.send(`__**${message.author.username}**__\nlevel:${res[0].level}\nprogress untill next level: **${res[0].points}/${(res[0].level * 100)}**`)
    })

}

module.exports.config = {
    name: "level",
    aliases: ["lvl"],
      usage: "!level",
    description: "see your level info",
    acclvl: 1
    }