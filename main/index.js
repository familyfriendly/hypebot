const Discord = require("discord.js");
const fs = require("fs");
const ini = require("ini");
const readline = require('readline');
const {database} = require("wacky-db");
var moment = require('moment');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});
var client = new Discord.Client();
client.commands = new Discord.Collection();
client.aliases = new Discord.Collection();
client.data = new database("./data.json");

if (!fs.existsSync("./config/settings.ini")) {
  let config = {
    token: null,
    prefix: "!",
    gtype:"WATCHING",
    gmsg:"you",
    moderation:
    {
      // ; please fill this in, else your moderators will be super duper sad. (id)
      modrole:null,

      // ; the channel the bot loggs moderation commands in (leave blank to disable)
      logging_channel:null,

      /*
; please only edit this if you know what regex is and what it does,
; for a quick guide check https://www.regular-expressions.info/
      */
      regex:"/fuck|nigga|nigger/gi",

      //; 1=message deleted, 2= message deleted + verbal warning from bot
      regex_triggered:1,

      //; the id of the role that will be assigned on mute
      mute_role:null,

      // ; the defualt time for a mute (if another time isnt specified)
      mute_defualt:"10m",
      /* 
      ; if the number of total warns for a user surpasses any auto_* command that action will be used on the user
      ; leave blank to disable 
      */
      auto_mute:null,
      auto_kick:null,
      auto_ban:null
    },
    database:{
      url:"¯\_(ツ)_/¯",
      password:"keep safe"
    }
  }
  console.log("\x1b[33m", 'no config file detected. likely first startup... \x1b[0m');

  rl.question('input token: ', (answer) => {
    config.token = answer;
    rl.close();
    console.log(`config data (can be changed by editing settings.ini)`);
    console.log(config);
    fs.appendFile(`${__dirname}/../config/settings.ini`, ini.stringify(config), (err) => {
      if (err) return console.error(err);
      console.log("succesfully saved")
    });
  });
}
fs.readdir(`${__dirname}/../commands`, (err, files) => {
  if (err) console.log(err)
  let jsfile = files.filter(f => f.split(".").pop() === "js")
  if (jsfile.length <= 0) {
    return console.log("lol no cmds")
  }
  jsfile.forEach((f, i) => {
    console.log(`command ${f} loaded...`)
    let pull = require(`${__dirname}/../commands/${f}`);
    client.commands.set(pull.config.name, pull)
    pull.config.aliases.forEach(alias => {
      client.aliases.set(alias, pull.config.name)
    })
  })
})



let config = ini.parse(fs.readFileSync(`${__dirname}/../config/settings.ini`, "utf-8"));



client.config = config;
client.action = function(user,type,message,reason) {
  if(!user || !type || !message) return;
  reason = reason ? reason : "no reason set"; 
  let channel = client.channels.get(config.moderation.logging_channel);
  if(!channel) return;
  else {
    let colour = {warn:0xe6e006,mute:0x3c3fb0,kick:0xe1710b,ban:0xe40707}
    const embed = {
      "color": colour[type],
      "fields": [
        {
          "name": `REASON`,
          "value": `${reason}`
        },
        {
          "name": `${type} - ${user.username}`,
          "value": `${user.username} (${user.id})`,
          "inline": true
        },
        {
          "name": `MODERATOR - ${message.author.username}`,
          "value": `${message.author.username} (${message.author.id})`
        }
      ]
    };
    channel.send({ embed });
  }
if(reason == "too many infractions."){}
else {
  var sql = `INSERT INTO \`warns\` (\`id\`, \`reason\`) VALUES ('', NULL), ("${user.id}", "${reason}")`;
  db.query(sql, function (err, result) {
    if (err) throw err;
  });

sql = `SELECT * FROM \`warns\` WHERE id = ${user.id}`
db.query(sql, function (err, result) {
  if (err) throw err;
  let args = [user.id,"too many infractions."]; 
  let cmdFile;
if(config.moderation.auto_mute && result.length == config.moderation.auto_mute)
{
  if(type == "mute") return;
  cmdFile = require(`${__dirname}/../commands/mute.js`);
   cmdFile.run(client, message, args);
}
if(config.moderation.auto_kick && result.length == config.moderation.auto_kick)
{
  cmdFile = require(`${__dirname}/../commands/kick.js`);
  cmdFile.run(client, message, args);
}
if(config.moderation.auto_ban && result.length == config.moderation.auto_ban)
{
  cmdFile = require(`${__dirname}/../commands/ban.js`);
  cmdFile.run(client, message, args);
}

 
});
}



}
//init db
const mysql = require("mysql");
const db = mysql.createConnection({
  host: config.database.url,
  user: "484hype",
  password: config.database.password,
  database: "484hype"
})

//connect db
db.connect((err) => {
  if (err) throw err;
  console.log("connected to database")
  client.db = db;
});


var logc;

client.on("ready", () => {
  console.log(`Client ready!\nprefix: ${config.prefix}`);
  client.user.setActivity(config.gmsg, { type: config.gtype })
  .then(presence => console.log(`Activity set to ${presence.game ? presence.game.name : 'none'}`))
  .catch(console.error);
  logc = client.channels.get(String(config.moderation.logging_channel));
})


client.talkedRecently = new Set();
client.on("message", (message) => {

  var prefix = config.prefix;

  var args = message.content.slice(prefix.length).trim().split(/ +/g);
  var command = args.shift().toLowerCase();
if(config.moderation.regex)
{
  var regex = new RegExp(config.moderation.regex,"gi");
  let match = message.content.match(regex);
  if(match) {
    /*
    ; 1=message deleted, 2= message deleted + verbal warning from bot
    */

    switch(config.moderation.regex_triggered)
    {
      case "1":
      message.delete();
      break;
      case "2":
      message.reply("hey! dont say that");
      message.delete();
      break;
    }
    return
  }
}

  if(!message.author.bot)
  {



    let sql = `SELECT * FROM \`userdata\` WHERE id=${message.author.id}`
    db.query(sql, function (err, result) {
      if (err) console.log(err);
      if((result+"") === "")
      {
        sql = `INSERT INTO \`userdata\` (\`id\`, \`money\`, \`level\`, \`points\`) VALUES ('${message.author.id}', '100', '1', '1');`
        db.query(sql, function (err, result) {
          if (err) console.log(err);
          console.log(`new user ${message.author.username} logged!`);
        });
      } else {
        let points = result[0].points + Math.floor(Math.random() * 30);

        if(points > (100 * result[0].level))
        {
          sql = `
UPDATE \`userdata\`
SET points = '0',  level = '${(result[0].level + 1)}'
WHERE id='${message.author.id}'
`
message.channel.send(`GG ${message.author.username}! you leveled up.`)
        } else
        {
          sql = `
          UPDATE \`userdata\`
          SET points = ${points}
          WHERE id='${message.author.id}'
          `
        }

db.query(sql, function (err, result) {
  if (err) console.log(err);
});
      }
    });

  }



  if (message.channel.type == "dm" || message.author.bot || !message.channel.permissionsFor(message.guild.me).has("SEND_MESSAGES") || message.content.indexOf(prefix) != 0) return;


  if(client.data.exists(message.author.id)) {
    let dda = client.data.get(message.author.id);
    dda = new Date(dda);
    dda = moment(dda.toISOString())
    if(moment(dda).isBefore(moment())) {
      message.member.removeRole("577232320277381121");
      message.channel.send(`**oH FRicK** ${message.author}. you seem to have run out of premium juice. get some more by boosting the server! (and make sure a mod/admin sees)`);
    }
  }

  let commandfile = client.commands.get(command) || client.commands.get(client.aliases.get(command))
  if (!commandfile) return;

  if(commandfile.config.acclvl === 1) {}
  else if (commandfile.config.acclvl === 2)
  {
    if(message.member.hasPermission('ADMINISTRATOR') ||message.member.roles.has(config.moderation.modrole)){}
    else return;
  }
  else if (commandfile.config.acclvl === 3) {
    if(message.member.hasPermission('ADMINISTRATOR')) {}
    else return;
  }
  try {
    let cmdFile = require(`${__dirname}/../commands/${commandfile.config.name}`);
    cmdFile.run(client, message, args);
    if(!logc) return console.warn(`!! logging channel ID incorrect or my permissions are set wrongfully.`);
    logc.send(`${message.author.username} used command ${commandfile.config.name} in ${message.channel} ${args ? `with the arguments \` ${args.join(" ")} \` `: "" }`)
  } catch (err) {
    if(!logc) return console.warn(`!! logging channel ID incorrect or my permissions are set wrongfully.`);
    logc.send(`encountered an error \`\`\` ${err} \`\`\` `)
  }
})


setInterval(() => {
let sql = `
SELECT * FROM \`comp\`
`

client.db.query(sql, function(err,rows) {
  if(err) return console.log(err);
  const base_income = 100;
  for(let i = 0; i < rows.length; i++)
  {
    let data = rows[i];
    data.json = JSON.parse(data.json)
    let income = base_income * data.json.upgrades
    sql = `
    UPDATE \`comp\` SET \`capital\` = \`capital\` + '${income}' WHERE \`comp\`.\`id\` = "${data.id}";   
    `
    client.db.query(sql, function(err) {
      if(err) console.log(`error paying out ${income} to ${data.name} `)

     })

  }

})

}, 3600000);

client.login(config.token);